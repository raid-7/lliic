package raid.hack.crypto.fantom;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import raid.hack.crypto.fantom.response.ExecutionResponse;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.TimeoutException;

import static raid.hack.crypto.fantom.CompilationAPI.SUPPORTED_FORMATS;

@RestController
public class AnalyzeCodeAPI {
    private static final String LLIIC_PATH = "../playground/run";
    private static final String PLAYGROUND_PATH = "../playground/";
    private static final long TIME_LIMIT = 40_000; // 40 seconds

    @PostMapping("/api/run-count-operations")
    public ExecutionResponse runCountOperations(@RequestParam(value = "code") String code,
                                                @RequestParam(value = "format") String format) {

        if (Arrays.stream(SUPPORTED_FORMATS).noneMatch(format::equals)) {
            return null;
        }

        String id = CoderHelper.nextIdentifier();
        File file = writeFile(id, format, code);
        try {
            return execute(file);
        } catch (TimeoutException exc) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "Execution timeout");
        } finally {
            cleanUp(file);
        }
    }


    private File writeFile(String fileId, String format, String code) {
        File file = new File(PLAYGROUND_PATH, fileId + "." + format);
        try (var writer = new BufferedWriter(new FileWriter(file))) {
            writer.write(code);
        } catch (IOException exc) {
            exc.printStackTrace();
        }
        return file;
    }

    private void cleanUp(File file) {
        file.delete();
    }

    private ExecutionResponse execute(File file) throws TimeoutException {
        try {
            return ExecutionController.runLimited(TIME_LIMIT, "sudo", "-u", "unsafe", LLIIC_PATH, file.getAbsolutePath());
        } catch (InterruptedException exc) {
            return null;
        }
    }
}
