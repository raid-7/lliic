InterfaceConsole = (function () {
    let _console = $('#console');
    let _data = [];
    let _lastFile = null;

    function _textify(text, jqElement) {
        jqElement.text(text);
        let html = jqElement.html();
        jqElement.html(html.replace(/\r?\n/g, '<br/>'));
        return jqElement;
    }

    function _displayOutput() {
        let data = _data;
        _console.children().remove();

        if (data.length === 0) {
            _console.addClass('unfilled');
        } else {
            _console.removeClass('unfilled');
            data.forEach(function (execData) {
                let title = execData.title;
                let stdout = execData.stdout;
                let stderr = execData.stderr;

                if (title && (stdout || stderr)) {
                    _console.append($('<h4 />').html(title));
                }
                if (stdout) {
                    _console.append(_textify(stdout, $('<div class="console-stdout" />')));
                }
                if (stderr) {
                    _console.append(_textify(stderr, $('<div class="console-stderr" />')));
                }
            });
        }
    }

    let exportInterface = {
        appendData: function (data) {
            if (!data)
                return;
            if (!data.forEach)
                data = [data];

            if (_lastFile != selectedFile) {
                _lastFile = selectedFile;
                _data = [];
            }

            for (let i = 0; i < data.length; i++)
                _data.push(data[i]);

            _displayOutput();

            return this;
        },
        setData: function (data) {
            return this.clear().appendData(data);
        },
        clear: function () {
            _lastFile = null;
            _data = [];
            _displayOutput();
            return this;
        }
    };

    exportInterface.clear();
    return exportInterface;
})();

InterfaceConsole.clear();


$('.compile-button').on('click', function () {
    if (selectedFile) {
        compileCurrentFile();
    }
});

$('.test-button').on('click', function () {
    if (selectedFile) {
        let file = new File(selectedFile);
        runBytecode(file);
    }
});


(function INIT_CONTRACT_TOOLBOX() {
    function getArgNames(element) {
        let attrs = element.dataset.args;
        if (!attrs)
            return 0;
        return attrs.split('|');
    }

    $('.contract-toolbox .method-button').on('click', function (ev) {
        if (!$(ev.target).hasClass('method-button'))
            return;

        let action = ev.target.dataset.action;
        let numArgs = getArgNames(ev.target).length;
        let attrs = new Array(numArgs);
        for (let i = 0; i < numArgs; i++) {
            attrs[i] = $(ev.target).find("input[data-index='" + i + "']").val();
            if (attrs[i] === "") {
                swal({
                    title: 'Fill all arguments',
                    icon: 'warning'
                });
                return;
            }
        }
        console.log(attrs);
        callSmartMethod(action, attrs);
    });

    $('.contract-toolbox .method-button').each(function () {
        let self = this;
        $(self).on('keypress', function (ev) {
            if (ev.which == 13) {
                $(self).click();
            }
        });
    });

    $('.contract-toolbox .method-button').each(function () {
        let argNames = getArgNames(this);
        for (let i = 0; i < argNames.length; i++) {
            $(this).children('span')
                .append($('<input type="text" data-index="' + i + '" placeholder="' + argNames[i] + '"/>'));
            if (i != argNames.length - 1) {
                $(this).children('span').append($('<span>, </span>'));
            }
        }
    });
})();


initFiles("development");
